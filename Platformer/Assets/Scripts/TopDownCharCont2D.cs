﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TopDownCharCont2D : MonoBehaviour {

    public float speed = 5.0f;
    public int jump = 10;
    public int frameCount = 10;
    private int count = 0;
    public Vector2 boundVelocity = new Vector2(50, 20);
    Rigidbody2D rigidbody2D;
    void Start()
    {
        rigidbody2D = GetComponent<Rigidbody2D>();
    }
    void FixedUpdate()
    {
        float x = Input.GetAxis("Horizontal");

        rigidbody2D.velocity = new Vector2((x * speed), rigidbody2D.velocity[1]) ;

        if (Input.GetAxis("Vertical") > 0 && rigidbody2D.velocity[1] == 0)
        {
            rigidbody2D.velocity = new Vector2((x * speed), Input.GetAxis("Vertical") * jump);
        }

    }
    
}
